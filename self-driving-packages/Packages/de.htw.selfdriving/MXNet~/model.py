import numpy as np
import mxnet as mx
from mxnet import gluon, nd, autograd, metric, init

# Simple CNN model with two convolutional layers, two pooling layers and
# two dense (fully connected) layers.
def build_net():
	net = gluon.nn.HybridSequential()
	with net.name_scope():
		net.add(gluon.nn.Conv2D(channels=28, kernel_size=11, activation='relu'))
		net.add(gluon.nn.MaxPool2D(pool_size=2, strides=2))
		net.add(gluon.nn.Conv2D(channels=56, kernel_size=5, activation='relu'))
		net.add(gluon.nn.MaxPool2D(pool_size=2, strides=2))
		net.add(gluon.nn.Flatten())
		net.add(gluon.nn.Dense(10))
		net.add(gluon.nn.Dense(2))

	return net
