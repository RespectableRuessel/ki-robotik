import os
import sys
import hashlib
import time
import numpy as np
import pandas as pd
from PIL import Image

# ------- Process Arguments -------

data_location = sys.argv[1]

# ------- Enlarge Dataset -------
ops_count = 1

record_data = pd.read_csv(os.path.join(data_location, "record_data.csv"))
record_data = record_data.sample(frac=1).reset_index(drop=True)
record_data_length = len(record_data.index)
record_data_enlarged = pd.DataFrame(index=np.arange(0, record_data_length * ops_count), columns=record_data.columns)

print('Loaded %s Records.' % (record_data_length))

# Mirror the images to enlarge the dataset and keep the network from memorizing
# the order of images.
def mirror_record_data(path, record):
	img = Image.open(os.path.join(path, record['imageFile']))
	img_mirror = img.transpose(Image.FLIP_LEFT_RIGHT)
	img_mirror_name = record['imageFile'].replace('.png', '_mirrored.png')
	img_mirror.save(os.path.join(path, img_mirror_name))
	record_data_enlarged.loc[i] = [img_mirror_name, record['motorTorque'], np.float32(record['steeringAngle']) * - 1.]

for i, row in record_data.iterrows():
	mirror_record_data(data_location, row)

	print('\rImage %s processed.' % (i), end='\r')

record_data_enlarged = pd.concat([record_data, record_data_enlarged])
record_data_enlarged.to_csv(os.path.join(data_location, "record_data.csv"), index=False)

print('Processing completed.')
