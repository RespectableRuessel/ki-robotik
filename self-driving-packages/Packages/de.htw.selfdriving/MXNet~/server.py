import sys
import os
import glob
import io
import base64
import json
import socketserver
import numpy as np
import mxnet as mx
from mxnet import gluon, nd, autograd
from http.server import HTTPServer, BaseHTTPRequestHandler
from PIL import Image
# --- local ---
import model
# -------------

# ------- Process Arguments -------

checkpoint = ''

if len(sys.argv) > 1:
	checkpoint = sys.argv[1]
	checkpoint_name = os.path.split(checkpoint)[1].replace('.params', '')
	print('Using Checkpoint %s.' % (checkpoint_name))

if not checkpoint:
	if not os.path.exists('checkpoints'):
		print('No Checkpoints found.')
		exit()

	available_checkpoints = glob.glob('checkpoints/*.params')
	checkpoint = max(available_checkpoints, key=os.path.getctime)
	checkpoint_name = os.path.split(checkpoint)[1].replace('.params', '')
	print('No Checkpoint specified. Using Checkpoint %s.' % (checkpoint_name))

# ------- Build Network -------

ctx = mx.cpu()

net = model.build_net()

net.load_parameters(checkpoint, ctx=ctx)
net.hybridize()

# Transpose the input dimension from height x width x rgba to rgba x height x width.
def format_img_arr(img_arr):
	return nd.array(np.transpose(img_arr, axes=(2, 0, 1)))

# Predict values for the given image.
def predict(img):
	img_arr = np.array(img).astype('float32')
	img_arr = format_img_arr(img_arr)
	img_arr = img_arr.reshape(1, img_arr.shape[0], img_arr.shape[1], img_arr.shape[2])
	output = net(img_arr).reshape(2).asnumpy()

	return output

# ------- RESTful Server -------

# Storage for the latest prediction.
prediction = np.array([0.0, 0.0])

# Simple RESTful server.
class MXNetRequestHandler(BaseHTTPRequestHandler):
	def _set_headers(self):
		self.send_response(200)
		self.send_header('Content-Type', 'application/json')
		self.end_headers()

	def do_HEAD(self):
		self._set_headers()

	def do_GET(self):
		self._set_headers()
		global prediction

		print('motorTorque: %s, steeringAngle: %s' % (prediction[0], prediction[1]))

		# Return a json with the latest prediction.
		self.wfile.write(json.dumps({'motorTorque': float(prediction[0]), 'steeringAngle': float(prediction[1])}).encode(encoding='utf-8'))

	def do_POST(self):
		content_type = self.headers['Content-Type']

		if not 'application/json' in content_type:
			self.send_response(400)
			self.end_headers()
			return

		# Read the json file and decode the image.
		length = int(self.headers['Content-Length'])
		message = json.loads(self.rfile.read(length))

		im = Image.open(io.BytesIO(base64.b64decode(message['image'])))

		# im.save("tmp.png") to test if a image can be read.

		# Make a prediction for the transmitted image.
		global prediction
		prediction = predict(im)
		print('Predicted %s.' % (prediction))

		self._set_headers()


port = 8008
address = ('', 8008)
httpd = HTTPServer(address, MXNetRequestHandler)

print('Starting httpd on Port %s.' % (port))
httpd.serve_forever()
