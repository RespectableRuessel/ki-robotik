import os
import sys
import hashlib
import time
import numpy as np
import pandas as pd
import mxnet as mx
from mxnet import gluon, nd, autograd, metric, init
from PIL import Image
# --- local ---
import model
# -------------

# ------- Process Arguments -------

epochs = int(sys.argv[1])
batch_size = int(sys.argv[2])
data_location = sys.argv[3]
checkpoint = ''

if len(sys.argv) > 4:
	checkpoint = sys.argv[4]
	checkpoint_name = os.path.split(checkpoint)[1].replace('.params', '')
	print('Using Checkpoint %s.' % (checkpoint_name))

# ------- Prepare Datasets -------

record_data = pd.read_csv(os.path.join(data_location, "record_data.csv"))
record_data = record_data.sample(frac=1).reset_index(drop=True)
record_data_length = len(record_data.index)

print('Loaded %s Records.' % (record_data_length))

# Prepare the image and label it.
def load_record_data(path, record):
	img = Image.open(os.path.join(path, record['imageFile']))
	img_arr = np.array(img).astype('float32')
	torque = np.float32(record['motorTorque'])
	angle = np.float32(record['steeringAngle'])
	label = np.array([torque, angle])

	return img_arr, label

# Transpose the input dimension from height x width x rgba to rgba x height x width.
def format_img_arr(img_arr):
	return nd.array(np.transpose(img_arr, axes=(2, 0, 1)))


train_length = int(record_data_length * 0.7)
test_length = record_data_length - train_length

print('Selecting %s Records as Train Dataset and %s Records as Test Dataset.' % (train_length, test_length))

X_train = []
y_train = []

X_test = []
y_test = []

for i, row in record_data.iterrows():
	img_arr, label = load_record_data(data_location, row)
	img_arr = format_img_arr(img_arr)

	if i < train_length:
		X_train.append(img_arr)
		y_train.append(label)
	else:
		X_test.append(img_arr)
		y_test.append(label)

# Build the train dataset.
train_dataset = gluon.data.dataset.ArrayDataset(X_train, y_train)
train_dataloader = gluon.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

# Build the test dataset.
test_dataset = gluon.data.dataset.ArrayDataset(X_test, y_test)
test_dataloader = gluon.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

# ------- Build Network -------

ctx = mx.cpu()

net = model.build_net()

if not checkpoint:
	net.initialize(init.Xavier(magnitude=2.24), ctx=ctx)
else:
	net.load_parameters(checkpoint, ctx=ctx)

net.hybridize()

# ------- Train Network -------

l2 = gluon.loss.L2Loss()
trainer = gluon.Trainer(params=net.collect_params(), optimizer='adam', optimizer_params={"learning_rate": 0.001})

train_rmse = metric.RMSE()
test_rmse = metric.RMSE()

for e in range(epochs):
	# Training loop.
	for i, (data, label) in enumerate(train_dataloader):
		with autograd.record():
			data = data.as_in_context(ctx)
			label = label.as_in_context(ctx)
			output = net(data)
			loss = l2(output, label)

		# Train the network.
		loss.backward()
		trainer.step(data.shape[0])
		train_rmse.update(label, output)

	# Test loop.
	for i, (data, label) in enumerate(test_dataloader):
		data = data.as_in_context(ctx)
		label = label.as_in_context(ctx)
		output = net(data)
		test_rmse.update(label, output)

	print("Epoch {}: train loss: {:.5f}, train RMSE {:.3f}, test RMSE {:.3f}".format(
		e, loss.mean().asscalar(), float(train_rmse.get()[1]), float(test_rmse.get()[1])))

# ------- Export Parameters -------

if not os.path.exists('checkpoints'):
	os.makedirs('checkpoints')

checkpoint_name = str(hex(int(time.time())))[2:]
checkpoint_file = os.path.join('checkpoints', checkpoint_name + '.params')
net.save_parameters(checkpoint_file)
print('Training Completed. Saving Network Parameters.')
print('Created Checkpoint %s.' % (checkpoint_name))
