using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Htw.SelfDriving.Controller
{
	[System.Serializable]
	public class AxleInfo
	{
		public WheelCollider leftWheel;
		public WheelCollider rightWheel;
		public bool motor;
		public bool steering;

		public void Apply(float torque, float angle)
		{
			if(this.steering)
			{
				this.leftWheel.steerAngle = angle;
				this.rightWheel.steerAngle = angle;
			}

			if(this.motor)
			{
				this.leftWheel.motorTorque = torque;
				this.rightWheel.motorTorque = torque;
			}
		}

		public void LocalPositionToVisuals()
		{
			Transform leftVisualWheel = this.leftWheel.transform.GetChild(0);
			Transform rightVisualWheel = this.rightWheel.transform.GetChild(0);

			Vector3 position;
			Quaternion rotation;

			this.leftWheel.GetWorldPose(out position, out rotation);
			leftVisualWheel.transform.position = position;
			leftVisualWheel.transform.rotation = rotation;

			this.rightWheel.GetWorldPose(out position, out rotation);
			rightVisualWheel.transform.position = position;
			rightVisualWheel.transform.rotation = rotation;
		}
	}

	public abstract class CarController : MonoBehaviour
    {
		[SerializeField]
		private float maxMotorTorgue;
		public float MaxMotorTorgue
		{
			get { return this.maxMotorTorgue; }
			set { this.maxMotorTorgue = value; }
		}

		[SerializeField]
		private float maxSteeringAngle;
		public float MaxSteeringAngle
		{
			get { return this.maxSteeringAngle; }
			set { this.maxSteeringAngle = value; }
		}

		[SerializeField]
		private List<AxleInfo> axleInfos;
		public List<AxleInfo> AxleInfos
		{
			get { return this.axleInfos; }
			set { this.axleInfos = value; }
		}

		public void Awake()
		{
			foreach(AxleInfo info in axleInfos)
			{
				// Let the car drive a bit better.
				info.leftWheel.ConfigureVehicleSubsteps(5 ,12, 15);
				info.rightWheel.ConfigureVehicleSubsteps(5 ,12, 15);
			}
		}

		public void FixedUpdate()
		{
			// Power the engine and rotate the axis.
			float torque = maxMotorTorgue * GetMotorAxis();
			float angle = maxSteeringAngle * GetSteeringAxis();

			foreach(AxleInfo info in axleInfos)
			{
				info.Apply(torque, angle);
				info.LocalPositionToVisuals();
			}
		}

		public abstract float GetMotorAxis();
		public abstract float GetSteeringAxis();
	}
}
