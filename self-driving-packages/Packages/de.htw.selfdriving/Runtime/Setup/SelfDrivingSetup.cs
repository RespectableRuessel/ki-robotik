using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Htw.SelfDriving.Autonomous;
using Htw.SelfDriving.Human;

namespace Htw.SelfDriving.Setup
{
	[RequireComponent(typeof(HumanCarController))]
	[RequireComponent(typeof(HumanCarRecorder))]
	[RequireComponent(typeof(AutonomousCarController))]
	[RequireComponent(typeof(AutonomousCarRecorder))]
	public class SelfDrivingSetup : MonoBehaviour
    {
		[SerializeField]
		private bool autonomousSession;
		public bool AutonomousSession
		{
			get { return this.autonomousSession; }
			set { this.autonomousSession = value; }
		}

		public void Awake()
		{
			base.GetComponent<HumanCarController>().enabled = !this.autonomousSession;
			base.GetComponent<HumanCarRecorder>().enabled = !this.autonomousSession;
			base.GetComponent<AutonomousCarController>().enabled = this.autonomousSession;
			base.GetComponent<AutonomousCarRecorder>().enabled = this.autonomousSession;
		}
	}
}
