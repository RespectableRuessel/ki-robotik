using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using UnityEngine;

namespace Htw.SelfDriving.Record
{
	public class Recorder
    {
		private string recordName;
		private DirectoryInfo recordDirectory;
		private FileInfo recordData;

		public Recorder(string recordName, string[] headers)
		{
			this.recordName = "Record_" + recordName;

			// Save the recording into (Windows) AppData/LocalLow/COMPANYNAME/PROJECTNAME/RECORDNAME
			this.recordDirectory = new DirectoryInfo(Path.Combine(Application.persistentDataPath, this.recordName));

			if(!this.recordDirectory.Exists)
				this.recordDirectory.Create();

			this.recordData = new FileInfo(Path.Combine(this.recordDirectory.FullName, "record_data.csv"));

			if(!this.recordData.Exists)
			{
				StringBuilder sb = new StringBuilder();

				for(int i = 0; i < headers.Length; ++i)
				{
					sb.Append(headers[i]);

					if(i < headers.Length - 1)
						sb.Append(",");
				}

				sb.Append("\n");

				File.WriteAllText(this.recordData.FullName, sb.ToString());
			}

		}

		public void Publish(RenderTexture rt, string[] parameters)
		{
			RenderTexture active = RenderTexture.active;

			// Create a new texture with the dimensions of the render texture.
			Texture2D texture = new Texture2D(rt.width, rt.height);
			RenderTexture.active = rt;

			// Render the camera image into the texture.
			texture.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
			texture.Apply();

			RenderTexture.active = active;

			// Use the current time tick as file name to keep the order.
			string ticks = DateTime.Now.Ticks.ToString();
			string fileName = ticks + ".png";
			string filePath = Path.Combine(this.recordDirectory.FullName, fileName);

			// Write the image to disk via png encoding.
			File.WriteAllBytes(filePath, texture.EncodeToPNG());

			// Store the data into the record file.
			using(StreamWriter sw = File.AppendText(this.recordData.FullName))
	        {
				StringBuilder sb = new StringBuilder();

				sb.Append(fileName);
				sb.Append(",");

				for(int i = 0; i < parameters.Length; ++i)
				{
					sb.Append(parameters[i]);

					if(i < parameters.Length - 1)
						sb.Append(",");
				}

	            sw.WriteLine(sb.ToString());
	        }
		}
	}
}
