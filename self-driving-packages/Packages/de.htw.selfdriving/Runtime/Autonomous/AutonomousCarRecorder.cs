using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Htw.SelfDriving.Controller;
using Htw.SelfDriving.MXNet;

namespace Htw.SelfDriving.Autonomous
{
	[System.Serializable]
	public struct AutonomousCarMessage
	{
		public int width;
		public int height;
		public string image;

		public AutonomousCarMessage(int width, int height, byte[] image)
		{
			this.width = width;
			this.height = height;
			this.image = Convert.ToBase64String(image);
		}
	}

	[RequireComponent(typeof(MXNetConnection))]
	[RequireComponent(typeof(CarController))]
	public class AutonomousCarRecorder : MonoBehaviour
    {
		[SerializeField]
		private RenderTexture recordTexture;
		public RenderTexture RecordTexture
		{
			get { return this.recordTexture; }
			set { this.recordTexture = value; }
		}

		[SerializeField]
		private int recordFrameRate;
		public int RecordFrameRate
		{
			get { return this.recordFrameRate; }
			set { this.recordFrameRate = value; }
		}

		private CarController carController;
		private MXNetConnection connection;
		private float frameStepTime;
		private float nextFrameTime;

		public void Awake()
		{
			this.carController = base.GetComponent<CarController>();
			this.connection = base.GetComponent<MXNetConnection>();
			this.frameStepTime = 1f / (float)this.recordFrameRate;
			this.nextFrameTime = Time.time;
		}

		public void Start()
		{
			this.connection.enabled = true;
		}

		public void LateUpdate()
		{
			Record();
		}

		private void Record()
		{
			if(Time.time < this.nextFrameTime)
				return;

			RenderTexture active = RenderTexture.active;

			// Create a new texture with the dimensions of the render texture.
			Texture2D texture = new Texture2D(this.recordTexture.width, this.recordTexture.height);
			RenderTexture.active = this.recordTexture;

			// Render the camera image into the texture.
			texture.ReadPixels(new Rect(0, 0, this.recordTexture.width, this.recordTexture.height), 0, 0);
			texture.Apply();

			RenderTexture.active = active;

			// Transmit the image to the server via the json format encoded with png and base64.
			AutonomousCarMessage message = new AutonomousCarMessage(texture.width, texture.height, texture.EncodeToPNG());
			this.connection.Post(JsonUtility.ToJson(message));

			this.nextFrameTime = Time.time + this.frameStepTime;
		}
	}
}
