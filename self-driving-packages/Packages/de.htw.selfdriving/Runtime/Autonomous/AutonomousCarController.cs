using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Htw.SelfDriving.Controller;
using Htw.SelfDriving.MXNet;

namespace Htw.SelfDriving.Autonomous
{
	[System.Serializable]
	public struct AutonomousCarResult
	{
		public float motorTorque;
		public float steeringAngle;

		public static AutonomousCarResult Initial => new AutonomousCarResult { motorTorque = 0f, steeringAngle = 0f };
	}

	[RequireComponent(typeof(MXNetConnection))]
	public class AutonomousCarController : CarController
    {
		[SerializeField]
		private int requestRate;
		public int RequestRate
		{
			get { return this.requestRate; }
			set { this.requestRate = value; }
		}

		private MXNetConnection connection;
		private AutonomousCarResult result;
		private float frameStepTime;
		private float nextFrameTime;

		public void OnEnable()
		{
			this.connection = base.GetComponent<MXNetConnection>();
			this.result = AutonomousCarResult.Initial;
			this.frameStepTime = 1f / (float)this.requestRate;
			this.nextFrameTime = Time.time;
		}

		public void Update()
		{
			if(Time.time > this.nextFrameTime)
			{
				// Retrieve the latest prediction from the server via json.
				string json = this.connection.Get();

				if(json != null)
					this.result = JsonUtility.FromJson<AutonomousCarResult>(json);

				this.nextFrameTime = Time.time + this.frameStepTime;
			}
		}

		public override float GetMotorAxis()
		{
			return this.result.motorTorque;
		}

		public override float GetSteeringAxis()
		{
			return this.result.steeringAngle;
		}
	}
}
