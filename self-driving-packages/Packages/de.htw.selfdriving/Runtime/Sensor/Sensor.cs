using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Htw.SelfDriving.Sensors
{
	public class Sensor : MonoBehaviour
	{
		private const float Radius = 0.25f;

		public bool Echo(float distance, out RaycastHit hit)
		{
			// Perform a simple sphere cast to simulate a distance sensor.
			RaycastHit rayHit;
			Ray ray = new Ray(transform.position, transform.forward);
			bool isHit = Physics.SphereCast(ray, Radius, out rayHit, distance);
			hit = rayHit;

#if UNITY_EDITOR
			if(isHit)
				Debug.DrawRay(transform.position, transform.forward * distance, Color.red, 0.05f);
			else
				Debug.DrawRay(transform.position, transform.forward * distance, Color.blue, 0.05f);
#endif

			if(isHit)
			    return true;

			return false;
		}
	}
}
