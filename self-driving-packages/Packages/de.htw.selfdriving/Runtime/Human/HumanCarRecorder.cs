using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Htw.SelfDriving.Controller;
using Htw.SelfDriving.Record;

namespace Htw.SelfDriving.Human
{
	[RequireComponent(typeof(CarController))]
	public class HumanCarRecorder : MonoBehaviour
    {
		[SerializeField]
		private string recordName;
		public string RecordName
		{
			get { return this.recordName; }
			set { this.recordName = value; }
		}

		[SerializeField]
		private RenderTexture recordTexture;
		public RenderTexture RecordTexture
		{
			get { return this.recordTexture; }
			set { this.recordTexture = value; }
		}

		[SerializeField]
		private int recordFrameRate;
		public int RecordFrameRate
		{
			get { return this.recordFrameRate; }
			set { this.recordFrameRate = value; }
		}

		[SerializeField]
		private GameObject recordVisual;
		public GameObject RecordVisual
		{
			get { return this.recordVisual; }
			set { this.recordVisual = value; }
		}

		private Recorder recorder;
		private CarController carController;
		private bool recording;
		private float frameStepTime;
		private float nextFrameTime;

		public void Awake()
		{
			this.recorder = new Recorder(this.recordName, GetParameterHeader());
			this.carController = base.GetComponent<CarController>();
			this.recording = false;
			this.frameStepTime = 1f / (float)this.recordFrameRate;
			this.recordVisual.SetActive(this.recording);
		}

		public void Update()
		{
			if(Input.GetKeyUp(KeyCode.R))
				ToggleRecordingState();

			if(this.recording)
				Record();
		}

		private void ToggleRecordingState()
		{
			this.recording = this.recording ? false : true;
			this.recordVisual.SetActive(this.recording);
			this.nextFrameTime = Time.time;
		}

		private void Record()
		{
			if(Time.time < this.nextFrameTime)
				return;

			// Publish the recorded data (image, torque, angle).
			this.recorder.Publish(this.recordTexture, GetParametersAsString());

			this.nextFrameTime = Time.time + this.frameStepTime;
		}

		private string[] GetParameterHeader()
		{
			return new string[]{
				"imageFile",
				"motorTorque",
				"steeringAngle"
			};
		}

		private string[] GetParametersAsString()
		{
			return new string[]{
				this.carController.GetMotorAxis().ToString(),
				this.carController.GetSteeringAxis().ToString()
			};
		}
	}
}
