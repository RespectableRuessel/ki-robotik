using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Htw.SelfDriving.Controller;

namespace Htw.SelfDriving.Human
{
	public class HumanCarController : CarController
    {
		public float MouseXAxis()
		{
			// Arma 3 like mouse controls for X axis.
			return Mathf.Lerp(-1f, 1f, Mathf.Max(0f, Input.mousePosition.x) / (float)Screen.width);
		}

		public float MouseYAxis()
		{
			// Arma 3 like mouse controls for Y axis.
			return Mathf.Lerp(-1f, 1f, Mathf.Max(0f, Input.mousePosition.y) / (float)Screen.height);
		}

		public override float GetMotorAxis()
		{
			return MouseYAxis();
		}

		public override float GetSteeringAxis()
		{
			return MouseXAxis();
		}
	}
}
