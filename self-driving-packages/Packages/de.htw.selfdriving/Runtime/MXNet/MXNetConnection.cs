using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using UnityEngine;

namespace Htw.SelfDriving.MXNet
{
	public class MXNetConnection : MonoBehaviour
    {
		[SerializeField]
		private string serverAddress;
		public string ServerAddress
		{
			get { return this.serverAddress; }
			set { this.serverAddress = value; }
		}

		private HttpClient client;

		public void Awake()
		{
			if(string.IsNullOrEmpty(this.serverAddress))
				this.serverAddress = "http://127.0.0.1:8008";
		}

		public void OnEnable()
		{
			this.client = new HttpClient();
		}

		public string Get()
		{
			// Connect to the server via get request.
			HttpResponseMessage response = client.GetAsync(this.serverAddress).Result;

			if(response.IsSuccessStatusCode)
			{
				// Return the json data result.
				HttpContent content = response.Content;
				return content.ReadAsStringAsync().Result;
			}

			return null;
		}

		public void Post(string data)
		{
			// Connect to the server via post request and transmit json data.
			HttpResponseMessage response = client.PostAsync(this.serverAddress, new StringContent(data, Encoding.UTF8, "application/json")).Result;
		}

		public void OnDisable()
		{
			if(this.client != null)
				this.client.Dispose();
		}
	}
}
