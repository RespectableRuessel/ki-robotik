using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Htw.GA.Training
{
	[RequireComponent(typeof(Supervisor))]
	public abstract class Policy : MonoBehaviour
	{
		public Supervisor Supervisor { get; set; }

		public virtual float Punish()
		{
			return 0f;
		}

		public virtual bool Violated()
		{
			return false;
		}

		public abstract void Clear();
	}
}
