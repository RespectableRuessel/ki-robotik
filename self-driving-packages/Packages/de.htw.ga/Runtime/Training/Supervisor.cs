using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Htw.GA.Training
{
	[RequireComponent(typeof(Agent))]
	public class Supervisor : MonoBehaviour
	{
		private Agent agent;
		private Policy[] policies;

		public void Awake()
		{
			this.agent = base.GetComponent<Agent>();
			this.policies = base.GetComponents<Policy>();

			foreach(Policy policy in this.policies)
				policy.Supervisor = this;
		}

		public void Start()
		{
			Restart();
		}

		public void Update()
		{
			// Checking if a policy is violated.
			foreach(Policy policy in this.policies)
			{
				if(policy.Violated())
				{
					Debug.Log(policy.GetType().Name + " violated. Starting next try.");

					// Punish the genome and restart.
					Score(-1f * Mathf.Abs(policy.Punish()));
					Restart();
					break;
				}
			}
		}

		public void Restart()
		{
			// Select the next genome and restart the agent.
			this.agent.Next();
			this.agent.Restart();

			foreach(Policy policy in this.policies)
				policy.Clear();
		}

		public void Score(float amount)
		{
			this.agent.Refresh(amount);
		}
	}
}
