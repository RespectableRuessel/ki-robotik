using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Htw.GA.Genetics;
using Htw.GA.Neurals;
using Htw.GA.Serialization;

namespace Htw.GA.Training
{
	public class Agent : MonoBehaviour
	{
		[SerializeField]
		private NetworkParameters networkParameters;
		public NetworkParameters NetworkParameters
		{
			get { return this.networkParameters; }
			set { this.networkParameters = value; }
		}

		[SerializeField]
		private int population;
		public int Population
		{
			get { return this.population; }
			set { this.population = value; }
		}

		[SerializeField]
		private float survivingRate;
		public float SurvivingRate
		{
			get { return this.survivingRate; }
			set { this.survivingRate = value; }
		}

		[SerializeField]
		private float mutationRate;
		public float MutationRate
		{
			get { return this.mutationRate; }
			set { this.mutationRate = value; }
		}

		[SerializeField]
		private float perpetuationRate;
		public float PerpetuationRate
		{
			get { return this.perpetuationRate; }
			set { this.perpetuationRate = value; }
		}

		[SerializeField]
		private bool saveFittest;
		public bool SaveFittest
		{
			get { return this.saveFittest; }
			set { this.saveFittest = value; }
		}

		[SerializeField]
		private UnityEvent onRestart;
		public UnityEvent OnRestart
		{
			get { return this.onRestart; }
			set { this.onRestart = value; }
		}

		[SerializeField]
		private UnityEvent onNextGeneration;
		public UnityEvent OnNextGeneration
		{
			get { return this.onNextGeneration; }
			set { this.onNextGeneration = value; }
		}

		private NNetwork network;
		private Species species;
		private int idx;
		private Genome genome;
		private float fitness;

		public void Awake()
		{
			this.network = new NNetwork(this.networkParameters);
			this.species = Species.Random(this.population, this.network.Weights.Count);
			this.species.SurvivingRate = this.SurvivingRate;
			this.species.MutationRate = this.MutationRate;
			this.species.PerpetuationRate = this.PerpetuationRate;
			this.idx = -1;

			if(this.onRestart == null)
				this.onRestart = new UnityEvent();

			if(this.onNextGeneration == null)
				this.onNextGeneration = new UnityEvent();
		}

		public void Restart()
		{
			this.fitness = 0f;
			this.onRestart.Invoke();
		}

		public void Refresh(float score)
		{
			this.fitness += score;
		}

		public void Next()
		{
			// Check for generation zero or a completed training of a genome.
			if(this.genome != null)
			{
				this.genome.Fitness = this.fitness;
				Debug.Log("Training for Genome " + this.idx + " completed. Fitness: " + this.genome.Fitness);
			} else {
				this.onNextGeneration.Invoke();
			}

			// Select the next genome.
			++idx;

			// Check if the training of all genomes finished and evolve the species.
			if(this.idx >= this.species.Population.Count)
			{
				// Search for the fittest genome in the population.
				Genome fittest = this.species.Fittest(1)[0];
				Debug.Log("Highest fittness for Generation " + this.species.Generation + " is " + fittest.Fitness);

				if(this.saveFittest)
				{
					Debug.Log("Saving fittest Genome.");
					Save(fittest, "generation_" + this.species.Generation);
				}

				// Evolve the species and reset the initial genome.
				this.species.Evolve();
				this.idx = 0;

				this.onNextGeneration.Invoke();
			}

			Debug.Log("Selecting Genome " + this.idx);
			this.genome = this.species.GenomeAt(this.idx);
			this.network.Weights = this.genome.Weights;
		}

		public List<float> Evaluate(List<float> inputs)
		{
			return this.network.Evaluate(inputs);
		}

		private void Save(Genome genome, string name)
		{
			// Save the genome into (Windows) AppData/LocalLow/COMPANYNAME/PROJECTNAME/Genomes
			DirectoryInfo directory = new DirectoryInfo(Path.Combine(Application.persistentDataPath, "Genomes"));

			if(!directory.Exists)
				directory.Create();

			// Serialize genome into json format.
			GenomeJson genomeJson = new GenomeJson();
			genomeJson.Save(Path.Combine(directory.FullName, name + ".json"), genome);
		}
	}
}
