using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Htw.GA.Genetics;

namespace Htw.GA.Serialization
{
	[System.Serializable]
	public class GenomeJson
	{
		[SerializeField]
		private float[] weights;

		public GenomeJson()
		{
		}

		public void Save(string file, Genome genome)
		{
			this.weights = genome.Weights.ToArray();
			File.WriteAllText(file, JsonUtility.ToJson(this, true));
		}

		public void Load(string file, out Genome genome)
		{
			JsonUtility.FromJsonOverwrite(File.ReadAllText(file), this);
			genome = new Genome(new List<float>(this.weights));
		}
	}
}
