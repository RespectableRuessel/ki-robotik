using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Htw.GA.Genetics
{
	public class Species
	{
		public List<Genome> Population { get; set; }
		public float SurvivingRate { get; set; }
		public float MutationRate { get; set; }
		public float PerpetuationRate { get; set; }
		public float Growth { get; set; }
		public int Generation { get; private set; }

		public static Species Random(int size, int weights)
		{
			List<Genome> genomes = new List<Genome>();

			for(int i = 0; i < size; ++i)
				genomes.Add(Genome.Random(weights));

			return new Species(genomes);
		}

		public Species(List<Genome> genomes, float survivingRate = 0.25f, float mutationRate = 0.1f, float perpetuationRate = 0.4f, float growth = 0f)
		{
		    this.Population = genomes;
			this.SurvivingRate = survivingRate;
			this.MutationRate = mutationRate;
			this.PerpetuationRate = perpetuationRate;
			this.Growth = growth;
			this.Generation = 0;
		}

		public void Evolve()
		{
			// Breen new population by survival of the fittest.
			List<Genome> genomes = Breed(this.SurvivingRate, this.MutationRate, this.PerpetuationRate);
			this.Population = genomes;
			++this.Generation;
		}

		public Genome GenomeAt(int index)
		{
			return this.Population[index];
		}

		public List<Genome> Fittest(int absolute)
		{
			return this.Population.OrderByDescending(genome => genome.Fitness).ToList().GetRange(0, absolute);
		}

		public List<Genome> Fittest(float percentage)
		{
			return Fittest(Mathf.FloorToInt(percentage * this.Population.Count));
		}

		public List<Genome> Breed(float surviving, float mutation, float perpetuation)
		{
			// Use the n (percentage) fittest genomes as parents.
			List<Genome> parents = Fittest(surviving);
			List<Genome> children = new List<Genome>();
			Genome fittest = parents[0];


			// Let the fittest survive.
			children.Add(fittest);
			children.Add(fittest.Mutate(mutation, perpetuation));

			// Breed babies with parents weights.
			List<Genome> babies;
			babies = Genome.CrossBreed(parents);

			// Mutate each baby.
			foreach(Genome baby in babies)
				children.Add(baby.Mutate(mutation, perpetuation));

			// Kill children so that the population remains constant.
			if(children.Count > this.Population.Count)
			{
				int toKill = children.Count - this.Population.Count;

				for(int i = 0; i < toKill; ++i)
					children.RemoveAt(children.Count - 1);
			}

			// Fill remaining slots with random children.
			int growth = Mathf.FloorToInt(this.Population.Count * this.Growth);
			int remaining = this.Population.Count + growth - children.Count;

			for(int i = 0; i < remaining; ++i)
				children.Add(Genome.Random(fittest.Weights.Count));

			return children;
		}
	}
}
