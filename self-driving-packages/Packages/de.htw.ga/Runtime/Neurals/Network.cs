using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Htw.GA.Neurals
{
	public class NNetwork
	{
		public List<Layer> Layers { get; set; }
		public int Inputs => Layers[0].Neurons[0].Weights.Count - 1;
		public int Outputs => Layers[Layers.Count - 1].Neurons.Count;
		public int Hidden => Layers.Count - 1;
		public int Neurons => Layers[0].Neurons.Count;
		public List<float> Weights
		{
			get
			{
				List<float> weights = new List<float>();

				foreach(Layer layer in this.Layers)
					foreach(float weight in layer.Weights)
						weights.Add(weight);

				return weights;
			}
			set
			{
				int index = 0;

				foreach(Layer layer in this.Layers)
				{
					layer.Weights = value.GetRange(index, layer.Weights.Count);
					index += layer.Weights.Count;
				}
			}
		}

		public NNetwork()
		{
		    this.Layers = null;
		}

		public NNetwork(int inputs, int outputs, int hidden, int neurons) : this()
		{
			this.Layers = new List<Layer>();

			// Adding the input Layer first.
			this.Layers.Add(new Layer(neurons, inputs));

			// Fully connected hidden layer.
			for(int i = 0; i < (hidden - 1); ++i)
				this.Layers.Add(new Layer(neurons, neurons));

			// Adding the output layer last.
			this.Layers.Add(new Layer(outputs, neurons));
		}

		public NNetwork(NetworkParameters parameters) : this(parameters.inputs, parameters.outputs, parameters.hiddenLayer, parameters.neurons)
		{
		}

		public List<float> Evaluate(List<float> input)
		{
			List<float> output = input;

			// Get the output of each layer.
			foreach(Layer layer in Layers)
				output = layer.Evaluate(output);

			return output;
		}

		public NNetwork Copy()
		{
			return new NNetwork(this.Inputs, this.Outputs, this.Hidden, this.Neurons);
		}
	}
}
