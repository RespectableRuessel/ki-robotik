using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Htw.GA.Neurals
{
	[System.Serializable]
	public struct NetworkParameters
	{
		public int inputs;
		public int outputs;
		public int hiddenLayer;
		public int neurons;
	}
}
