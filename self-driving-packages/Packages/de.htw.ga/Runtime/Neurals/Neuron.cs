using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Htw.GA.Neurals
{
	public class Neuron
	{
		public List<float> Weights { get; set; }
		public float Bias
		{
			get { return this.Weights[this.Weights.Count - 1]; }
			set { this.Weights[this.Weights.Count - 1] = value; }
		}

		public static float Tanh(float value)
		{
			return (float)Math.Tanh(value);
		}

		public static float RandomClamped()
		{
			return UnityEngine.Random.Range(-1f, 1f);
		}

		public Neuron()
		{
			this.Weights = null;
		}

		public Neuron(List<float> weights) : this()
		{
			this.Weights = weights;
		}

		public Neuron(int inputs) : this()
		{
			this.Weights = new List<float>();

			// Initialize weights with random values.
			for(int i = 0; i < inputs; ++i)
				this.Weights.Add(RandomClamped());

			this.Weights.Add(RandomClamped());
		}

		public float Activate(List<float> input)
		{
			// Weights[... n - 1] is the bias neuron, so the input size must be length(weights) - 1.
			if((this.Weights.Count - 1) != input.Count)
				throw new System.ArgumentException(String.Format("{0} is different than {1}.", this.Weights.Count - 1, input.Count), "input");

			float stimulation = 0f;

			for(int i = 0; i < (this.Weights.Count - 1); ++i)
				stimulation += input[i] * this.Weights[i];

			// Run activation function on the sum of weights multiplied by the input.
			return Tanh(stimulation - this.Bias);
		}
	}
}
