# List of Papers and Resources used for the Implementation

## Genetic Algorithms
* [An Introduction to Genetic Algorithms](https://www.whitman.edu/Documents/Academics/Mathematics/2014/carrjk.pdf)
* [Genetic Algorithm for Solving Simple Mathematical Equality Problem](https://arxiv.org/ftp/arxiv/papers/1308/1308.4675.pdf)
* [Reinforcement learning without gradients: evolving agents using Genetic Algorithms](https://towardsdatascience.com/reinforcement-learning-without-gradients-evolving-agents-using-genetic-algorithms-8685817d84f)
* [Evolutionary Algorithms](http://ml.informatik.uni-freiburg.de/former/_media/teaching/ss13/ml/riedmiller/evolution.pdf)

## Convolutional Neural Networks (MXNet Gluon)
* [ImageNet Classification with Deep ConvolutionalNeural Networks](https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf)
* [Object Detection Using Convolutional Neural Networks](https://gluon.mxnet.io/chapter08_computer-vision/object-detection.html)
