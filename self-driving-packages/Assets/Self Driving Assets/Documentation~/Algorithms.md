# Algorithms

## Genetic Algorithm (GA)
>***Note***: The implementation of the genetic algorithm is based loosely on
[this](https://www.whitman.edu/Documents/Academics/Mathematics/2014/carrjk.pdf) paper.

First of all... what is a genetic algorithm? Genetic algorithm is a general term
for optimization algorithms that are inspired by nature or a biological process.
The basic idea is that you can find a optimal solution for a problem (in this case
a neural network for self driving cars) by a evolutionally approach.

#### Surivial of the Fittest
This implies that you can reward good solutions and punish bad solutions because
every solution survives by a chance of that score (also called fitness).
To take advantage of this process we need to define genomes (also called chromosomes)
that make up a single solution to our problem.
Every genome gets tested in our problem
world and earns a score. Multiple genomes are called a population and every species
has a population which is a result of a evolving generation of genomes.
At the end of every generation the selection operator chooses the best (based on the fitness) *n* genomes.
Every genome of that selection breeds children with the help of a crossover operator. The process
is sometimes called survival of the fittest.
This operator takes two genomes and swaps out the bits (also called weights) from a specific range.
The bits on the left side of genome *a* are replaced with the bits on the left side of genome *b* for
child *a* and the bits on the right side of genome *a* are replaced with the bits on the right side of genome *b*
for child *b*.

#### Mutation
Because the fitness is only measured for a single generation we need to mutate specific bits
so that the bits of the best genomes differ more every generation. Also mutation is important to ensure that the
next generation does not stuck on a local minimum. So how does the mutation work? The mutation itself is
a simple operator that selects a bit of a genome with the chance *p* and multiplies its value by
a random value combined with a perpetuation rate (between 0 and 1 to reduce the impact of the mutation).

#### The Cycle Repeats
After every operator was applied a new population can be assigned to our current species and the cycle repeats until the fitness of all genomes converges to one value or a perfect solution is found. Sometimes a high mutation rate can cause
a oscillation of fitness values between genomes of a population because even when a good solution is found it gets altered to much for the next generation so that is becomes a bad one. Also the result of the species depends on how good you are choosing your scoring function. For a larger overview of the topic look into [this](https://towardsdatascience.com/reinforcement-learning-without-gradients-evolving-agents-using-genetic-algorithms-8685817d84f) article.


##### Crossover Example with Mutation
with the split at m / 2 and values of 0 or 1.

###### Genome A
Bit   | 0 | 1 | 2 | 4
------|---|---|---|---
Value | 0 | 0 | 0 | 0

###### Genome B
Bit   | 0 | 1 | 2 | 4
------|---|---|---|---
Value | 1 | 1 | 1 | 1

###### Child A
Bit   | 0 | 1 | 2 | 4
------|---|---|---|---
Value | 0 | 0 | 1 | 1

###### Child B
Bit   | 0 | 1 | 2 | 4
------|---|---|---|---
Value | 1 | 1 | 0 | 0

###### Child A Mutated on Bit 2
Bit   | 0 | 1 | 2 | 4
------|---|---|---|---
Value | 0 | 0 | 0 | 1

## Convolutional Neural Network (CNN)
Like the genetic algorithms CNN's are also inspired by biological processes.
CNN's are part of a supervised learning process mainly used for image recognition / analysis and
natural language processing. The idea is that a basic fully connected neural network gets extended by
a convolutional layer that runs filter matrices on the network input similar to the neuron pattern
in the visual cortex of a animal. The weights of the convolutional network can be optimized with a gradient descent
algorithm like the fully connected network. Also pooling layers can be added to reduce the computing time of the learning
process. To get a understanding of how CNN's are working and how to implement one the [MXNet documentation](https://gluon.mxnet.io/chapter08_computer-vision/object-detection.html) is a good starting point.
