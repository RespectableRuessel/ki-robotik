# Installation

## Packages
The following packages need to be installed if they do not exist in your ```Packages/``` folder:

* de.htw.ga
* de.htw.selfdriving

To install these packages just copy the folders into ```Packages/``` inside your
root Unity project folder. Now start Unity.

## Requirements
* Unity 2018.3
* Python 3.7 or higher
* MX Net compatible CPU

## Unity
Import the project and open one of the scenes inside ```Self Driving Assets/Scenes```.
To test only the CNN open ```Only CNN Demo.unity``` and for the GA open ```Only GA Training.unity```.
If you want to test the full feature set open ```GA and CNN Training.unity```.

## Python
The following Python packages are required:

* numpy ```pip install numpy```
* mxnet ```pip install mxnet```
* pandas ```pip install pandas```
* pillow ```pip install pillow```

Now you should be able to start the ```server.py``` inside ```Packages/de.htw.selfdriving/MXNet~``` using ```py.exe server.py```.
