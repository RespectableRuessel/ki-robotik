# How to Use

## Only CNN Demo
>***Note***: This demo needs a running ```server.py```.

The scene can be run in to different ways: the *Recording Mode* and the *Testing Mode*.
To change the mode select the ```SkyCar``` inside the hierarchy.

### Recording Mode
To enable the *Recording Mode* turn off the *Autonomous Session* on the *Self Driving Setup* component.
Now you can start the scene and record images along your mouse input.
To control the car move your mouse (forward / backwards for speed, left / right to turn).
If you want to create a new *Recording* change the *Record Name* on the *Human Car Recorder* component.
Also you can set the frame rate of the recorded images and the size of the image with the *Render Image*.
When you are ready to start a new *Record* go to the play mode and hit the **r** key.
You should the a green box indicating that you are recording. Press **r** again to stop the record.
On Windows you can open ```AppData/LocalLow/COMPANYNAME/PROJECTNAME/RECORDINGNAME/``` to see the result of the record.
All images and values are saved there.

### Testing Mode
To enable the *Testing Mode* turn the *Autonomous Session* on also the server needs to be running.
The car should move on its own because the server returns the predictions of the camera images.
If you want to now the values of the predictions look at the server terminal. Every **GET** request tries to
get new predictions and every **POST** request transmits the current image of the camera to the neural network.

## Train with the Recorded Data
If you want to train the network with your newly created records open up the ```MXNet~``` folder.
Now copy the location of the root folder of your *Record* (like ```AppData/LocalLow/COMPANYNAME/PROJECTNAME/RECORDINGNAME```).
Open up a terminal window (or the PowerShell) and start the training process via: ```py.exe train.py 10 48 AppData/LocalLow/COMPANYNAME/PROJECTNAME/RECORDINGNAME``` with 10 as the epoch count and 48 as the batch size.
If you want to enlarge your dataset by mirroring the images use the ```enlarge_dataset.py``` on your record folder by typing: ```py.exe enlarge_dataset.py AppData/LocalLow/COMPANYNAME/PROJECTNAME/RECORDINGNAME```. Now the dataset should be two times larger.
Every finished training saves the parameters into a checkpoint located in ```checkpoints```. The checkpoints can be loaded by
appending the checkpoint location to the ```train.py``` like: ```py.exe train.py 10 48 AppData/LocalLow/COMPANYNAME/PROJECTNAME/RECORDINGNAME checkpoints/000000.params```
Checkpoints can be loaded on the server the same way (if not provided the server searches for the latest checkpoint): ```py.exe server.py checkpoints/000000.params```

## Only GA Training
Everything should be ready by default. Just start the play mode and watch the ```SkyCar```.
The lines in the scene indicate that the sensors have registered a collision. If you want to change the
network architecture take a look at the *Agent* component. Notice that the input size and the sensor size should be equal (so feel free to create sensors and adding them to the *Sensor Manager*).
The output size is 2 because they define the motor torque and the steering angle.
When *Save Fittest* is selected the fittest genome of a generation gets saved into a JSON format. Its located under ```AppData/LocalLow/COMPANYNAME/PROJECTNAME/Genomes```.
The *Suriving Rate* defines how many percent of the best genomes in a  population should survive. The *Mutation Rate* is the chance in percent
that a weight of a surviving genome gets mutated by the factor defined by the *Perpetuation Rate*.

## GA and CNN Training
>***Note***: This demo needs a running ```server.py```.

This scene is basically a combination of the other ones. After the CNN network has been trained you can start this scene like the
*Only GA Training* scene with the difference that the input size is the sensor size + 2. Thats because the result of the CNN is feeded into
the input of the GA network. Keep this in mind when changing the input size.
