![Architecture](architecture.png)

Basically the architecture is roughly split into three parts based on two packages.
The ```de.htw.ga``` package contains everything that belongs to the GA network.
It also has an *Agent* that provides access to the network and genome currently in use.
The *Species* evolves over time generating new populations of genomes that get evaluated by
the *Supervisor* (survival of the fittest). A *Supervisor* controls the fitness by special *Policies*. These *Policies* can give rewards or punishments to the active genome if the conditions defined in the *Policy* are met.
Everything that has to do with the car itself or the connection to MXNet is located inside the ```de.htw.selfdriving``` package. It provides a basic *Car Controller* that requires a input for the motor torque and a input for the steering angle.
The *Sensor Manager* manages all the *Sensor* components and updates them every frame.
Both packages combined allow a easy access to the CNN as wells as the GA.
Every custom script which are implementations of the package components are located in the Asset folder under ```Self Driving Assets/Scripts```.
