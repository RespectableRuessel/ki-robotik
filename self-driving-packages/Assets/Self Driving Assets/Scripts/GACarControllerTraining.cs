using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Htw.SelfDriving.Controller;
using Htw.SelfDriving.Sensors;
using Htw.GA.Training;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Agent))]
[RequireComponent(typeof(SensorManager))]
public class GACarControllerTraining : CarController
{
	[SerializeField]
	private Transform[] spawns;
	public Transform[] Spawns
	{
		get { return this.spawns; }
		set { this.spawns = value; }
	}

	private Rigidbody rigid;
	private Agent agent;
	private SensorManager sensorManager;
	private List<float> outputs;
	private Vector3 initialPos;
	private Quaternion initialRot;
	private Vector3 initialVelocity;

	public void Start()
	{
		this.rigid = base.GetComponent<Rigidbody>();
		this.agent = base.GetComponent<Agent>();
		this.sensorManager = base.GetComponent<SensorManager>();
		this.outputs = new List<float>(new float[2]);
		SetInitialState();

		this.agent.OnRestart.AddListener(InitialState);
		this.agent.OnNextGeneration.AddListener(SwitchSpawn);
	}

	public void Update()
	{
		// Evaluate the output of the network with the sensor data.
		this.outputs = this.agent.Evaluate(this.sensorManager.AcquireData());
	}

	public void InitialState()
	{
		// Reset the car to its initial state (... the spawn).
		transform.position = this.initialPos;
		transform.rotation = this.initialRot;
		this.rigid.velocity = this.initialVelocity;
	}

	public void SwitchSpawn()
	{
		// Select a random spawn to prevent the network from memorizing a specific route.
		int index = Random.Range(0, spawns.Length);
		Transform spawn = this.spawns[index];

		Debug.Log("Switching Spawn position to " + index + ".");

		transform.position = spawn.position;
		transform.rotation = spawn.rotation;

		SetInitialState();
		InitialState();
	}

	public override float GetMotorAxis()
	{
		return this.outputs[0];
	}

	public override float GetSteeringAxis()
	{
		return this.outputs[1];
	}

	private void SetInitialState()
	{
		// Use the current state as the new initial state.
		this.initialPos = transform.position;
		this.initialRot = transform.rotation;
		this.initialVelocity = this.rigid.velocity;
	}
}
