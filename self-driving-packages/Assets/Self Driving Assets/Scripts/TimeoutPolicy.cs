using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Htw.GA.Training;

[RequireComponent(typeof(DistancePolicy))]
[RequireComponent(typeof(CrashPolicy))]
public class TimeoutPolicy : Policy
{
	[SerializeField]
	private float timeout;
	public float Timeout
	{
		get { return this.timeout; }
		set { this.timeout = value; }
	}

	private DistancePolicy distancePolicy;
	private CrashPolicy crashPolicy;
	private float nextTimeout;

	public void Awake()
	{
		this.distancePolicy = base.GetComponent<DistancePolicy>();
		this.crashPolicy = base.GetComponent<CrashPolicy>();
	}

	public void Update()
	{
		// Check if the car is doing something or crashed.
		if(this.distancePolicy.Moved() || this.crashPolicy.Crashing())
		{
			this.nextTimeout = Time.time + this.timeout;
		}
	}

	public override float Punish()
	{
		return 10f;
	}

	public override bool Violated()
	{
		// The policy is violated when the car is not moving for x seconds.
		return Time.time > this.nextTimeout;
	}

	public override void Clear()
	{
		this.nextTimeout = Time.time + this.timeout;
	}
}
