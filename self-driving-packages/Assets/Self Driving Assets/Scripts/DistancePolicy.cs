using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Htw.GA.Training;

public class DistancePolicy : Policy
{
	private const float TimeStep = 1f;
	private const float BigFactor = 3f;

	[SerializeField]
	private float minDistance;
	public float MinDistance
	{
		get { return this.minDistance; }
		set { this.minDistance = value; }
	}

	private Vector3 lastPos;
	private Vector3 lastPosBig;
	private float nextTime;
	private float nextTimeBig;
	private float distance;
	private float distanceBig;

	public void Update()
	{
		// Check the distance every second.
		if(Time.time > this.nextTime)
		{
			// Calculate the distance the car has driven in x seconds.
			this.distance = Vector3.Distance(this.lastPos, transform.position);
			this.nextTime = Time.time + TimeStep;
			this.lastPos =  transform.position;

			// Reward the car for driving x meters.
			base.Supervisor.Score(this.distance);
		}

		// Check the distance over a long period to prevent driving in a circle.
		if(Time.time > this.nextTimeBig)
		{
			// Calculate the distance the car has driven in x seconds.
			this.distanceBig = Vector3.Distance(this.lastPosBig, transform.position);
			this.nextTimeBig = Time.time + TimeStep * 10f;
			this.lastPosBig = transform.position;
		}
	}

	public bool Moved()
	{
		return this.distance > this.minDistance && this.distanceBig > this.minDistance * BigFactor;
	}

	public override void Clear()
	{
		this.lastPos = transform.position;
		this.lastPosBig = transform.position;
		this.nextTime = Time.time + TimeStep;
		this.nextTimeBig = Time.time + TimeStep * 10f;
		this.distance = 0f;
		this.distanceBig = this.minDistance * BigFactor + 1f;
	}
}
