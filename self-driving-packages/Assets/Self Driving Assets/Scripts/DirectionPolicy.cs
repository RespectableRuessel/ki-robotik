using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Htw.GA.Training;

public class DirectionPolicy : Policy
{
	[SerializeField]
	private float maxBackwardTime;
	public float MaxBackwardTime
	{
		get { return this.maxBackwardTime; }
		set { this.maxBackwardTime = value; }
	}

	private Vector3 lastPos;
	private Vector3 lastForward;
	private bool backward;
	private float backwardTime;

	public void Update()
	{
		Vector3 movement = transform.position - this.lastPos;

		// Check if the car is driving backwards.
		if(Vector3.Dot(this.lastForward, movement) < 0)
		{
			if(!this.backward)
			{
				// Save when the car started to drive backwards.
				this.backward = true;
				this.backwardTime = Time.time;
			}
		} else {
			this.backward = false;
		}
	}

	public void LateUpdate()
	{
		this.lastPos = transform.position;
		this.lastForward = transform.forward;
	}

	public override float Punish()
	{
		return 5f;
	}

	public override bool Violated()
	{
		// The policy is violated when the car is driving backwards for x seconds.
		return this.backward && Time.time > this.backwardTime + this.maxBackwardTime;
	}

	public override void Clear()
	{
		this.lastPos = transform.position;
		this.lastForward = transform.forward;
		this.backward = false;
		this.backwardTime = 0f;
	}
}
