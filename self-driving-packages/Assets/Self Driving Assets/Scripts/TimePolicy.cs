using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Htw.GA.Training;

public class TimePolicy : Policy
{
	private float nextTime;

	public void Update()
	{
		if(Time.time > this.nextTime)
		{
			this.nextTime = Time.time + 1f;

			// Reward the car for living x seconds.
			base.Supervisor.Score(1f);
		}
	}

	public override void Clear()
	{
		this.nextTime = Time.time + 1f;
	}
}
