using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Htw.GA.Training;

[RequireComponent(typeof(Rigidbody))]
public class CrashPolicy : Policy
{
	[SerializeField]
	private float maxCrashTime;
	public float MaxCrashTime
	{
		get { return this.maxCrashTime; }
		set { this.maxCrashTime = value; }
	}

	[SerializeField]
	private float maxCrashCount;
	public float MaxCrashCount
	{
		get { return this.maxCrashCount; }
		set { this.maxCrashCount = value; }
	}

	private bool crashed;
	private float crashTime;
	private int crashCount;

	public void OnCollisionEnter()
	{
		// Add the current crash and start the timer.
		++this.crashCount;
		this.crashed = true;
		this.crashTime = Time.time;

		// Punish the car for every crash.
		base.Supervisor.Score(-5f);
	}

	public void OnCollisionExit()
	{
		this.crashed = false;
	}

	public bool Crashing()
	{
		return this.crashed;
	}

	public override bool Violated()
	{
		// The policy is violated when the car has crashed n times or is still in a crash for x seconds.
		bool crashTimeout = this.crashed && Time.time > this.crashTime + this.maxCrashTime;
		bool crashLimit = this.crashCount > this.maxCrashCount;
		return crashTimeout || crashLimit;
	}

	public override void Clear()
	{
		this.crashed = false;
		this.crashTime = 0f;
		this.crashCount = 0;
	}
}
